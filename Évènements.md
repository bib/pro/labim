# Évènements

## Rencontre avec Labase
__4 mars @Lebib__

## Rencontre, échanges et organisation
_7 mars @Latendresse 16h-18h_

## Ma super prez en 20 minutes
_5 mars  @LafabriqueENSAM 18h-19h30_
Description du contenu : si vous pensez comme Edward Tufte que Power Point rend idiot, et que vous voulez vous démarquer en présentation publique, nous vous proposons de réaliser en 20 minutes une présentation qui déchire en utilisant un éditeur de texte et un terminal, avec très peu de compétences informatiques. Nous parlerons de Markdown, HTML5, Pandoc et Reveal.js.

## Formats structurés et gestion de versions
_12 mars @LafabriqueENSAM 18h-19h30_
Description du contenu : nous manipulons des quantités de plus en plus grandes de documents numériques, nous avons parfois besoin de les convertir, parfois de retrouver une version antérieure, bref, nous avons besoin de manipuler ces documents sans reprendre la mise en forme. Apprendre à travailler avec des formats structurés et des gestionnaire de version assure la pérennité et l'interopérabilité des informations.

## Documentation du brassage de la BibMate@lebib
_dimanche 22 mars_

## Atelier 
_samedi 4 avril 14h/18h @lebib_
 Mise en place de boites de dépôt et documentation du projet documentation. Si on es suffisamment nombreux, on peut aussi en profiter pour remettre en route la Bib.lio.

## Workshops : PagedJs - ASCIIDOC
_25 avril @Latendresse 14h-20h_
On acceuille @john_tax et @oncletom pour deux workshops. [Pagedjs](https://www.pagedjs.org/) et [asciidoc](http://asciidoc.org/). Venez avec des cas concrets, si possible un peu réfléchis à l'avance. On pourra travailler par groupes sur différents projet. Par exemple un template pour imprimer des programmes du bib bien fichus en un clic depuis l'agenda en ligne :)
