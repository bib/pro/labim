LABASE : Laboratoire Autonome de Biologie Alternatif, Solidaire et Expérimental est un lieu d’expérimentation en biologie avec des objectifs de création, d’autonomie, d'auto-production et d’émancipation.

Des outils de documentation simples, solides et efficaces sont nécessaires pour faire vivre et péréniser les actions menées par Labase en particulier, mais aussi LeBiB.

Dés 2018 un premier projet était formulé comme suit.

#  Indexation des ressources (expériences et littérature)

Des outils numériques permettant le référencement, le construction et la mise à disposition de documentations relatives au projet seront mis en place. Ces outils devront respecter les règles du logiciel libre et
permettre une grande interopérabilité notamment pour offrir des possibilités de publication multiformes et d’échanges maitrisés.

Ils devront être hébergés dans un lieu physique proche via un prestataire de confiance, et n’inclure aucun service privateur, ni recourir à des prestataires faisant usage des données collectées.

## referencement
Un état des lieux de l'existant sera établi en amont, mais également au cour du projet par les équipes de travail. Référencée au format .bib, cette bibliographie devra inclure des metadonnées pour permettre un classement et une exploration croisée selon les besoins spécifiques à chaque projet.
Les fichiers .bib seront stockés via un systéme de gestion de version distribué de type GIT et accessible publiquement.

## construction
Les documentations relatives au déroulement du projet seront construites avec un langage de balisage léger, Markdown est aujourd'hui le format retenu, mais cela pourra évoluer. La simplicité d'utilisation et les possibilitées d'interaction sontles principaux arguments de choix.

La construction de ces documentations pourra se faire sur un wiki ou via un systéme de gestion de version distribué.

Une formation d'une demi journée pourra être mise en place pour les participants ne connaissant pas le langage retenu.

## mise à disposition
Les bibliographie ainsi que les documentations seront mises à disposition sur un site internet (https) et via le protocole OPDS.
Les outils permettant cette double publication devront être choisis en fonction des critéres suivants :

- simplicité d'utilisation,
- interopérabilité,
- licence libre

Les outils suivants sont envisagés :

- git
- dokuwiki
- wiki.js
- calibre

Une documentation des outils de référencement, documentation et mise à disposition devra également être mise à disposition.

## hébergement
Ces outils seront regroupés et mis à disposition sur un serveur dont l'emplacement physique devra se situer dans un rayon maximum de 300 km du lieu de travail. Il est envisageable de recourir à un prestataire ou de mettre en place un tel hébergement dans les locaux du bib hackerspace.

Le prestataire sera sélectionné selon les critéres suivants :

- Ne pas exploiter les données collectées à des fins personelles ou commerciales
- Permettre un accès SSH







